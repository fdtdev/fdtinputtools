﻿using UnityEditor;
using UnityEngine;

namespace com.FDT.InputTools.Editor
{
    [CustomPropertyDrawer(typeof(TouchEventsData))]
    public class TouchEventsDataDrawer:PropertyDrawer
    {
        protected SerializedProperty callbacksTargetProp;
        
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            callbacksTargetProp = property.FindPropertyRelative("callbacksTarget");
            
            EditorGUI.BeginProperty(position, label, property);
           
            GUI.Box(new Rect(position.x, position.y, position.width,
                        position.height), GUIContent.none);
          
            position.x += 2;
            position.width -= 4;
            position.y += 2;
            position.height -= 4;
            
            EditorGUI.PropertyField(new Rect(position.x, position.y, position.width, EditorGUIUtility.singleLineHeight), callbacksTargetProp, true);
            
            EditorGUI.EndProperty();
        }

        public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
        {
            return EditorGUIUtility.singleLineHeight + 4;
        }
    }
}