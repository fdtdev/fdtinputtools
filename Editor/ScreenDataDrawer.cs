﻿using UnityEditor;
using UnityEngine;

namespace com.FDT.InputTools.Editor
{
    [CustomPropertyDrawer(typeof(ScreenData))]
    public class ScreenDataDrawer:PropertyDrawer
    {
        protected SerializedProperty widthSafeAreaProp;
        protected SerializedProperty heightSafeAreaProp;
        
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            widthSafeAreaProp = property.FindPropertyRelative("widthSafeArea");
            heightSafeAreaProp = property.FindPropertyRelative("heightSafeArea");
            
            EditorGUI.BeginProperty(position, label, property);
           
            GUI.Box(new Rect(position.x, position.y, position.width,
                position.height), GUIContent.none);
          
            position.x += 2;
            position.width -= 4;
            position.y += 2;
            position.height -= 4;
            EditorGUI.LabelField(new Rect(position.x, position.y, position.width, EditorGUIUtility.singleLineHeight), label, EditorStyles.boldLabel);
            EditorGUI.PropertyField(new Rect(position.x, position.y + EditorGUIUtility.singleLineHeight + 2, position.width, EditorGUIUtility.singleLineHeight), widthSafeAreaProp, true);
            EditorGUI.PropertyField(new Rect(position.x, position.y + (EditorGUIUtility.singleLineHeight * 2) + 4 , position.width, EditorGUIUtility.singleLineHeight), heightSafeAreaProp, true);
            EditorGUI.EndProperty();
        }

        public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
        {
            return (EditorGUIUtility.singleLineHeight * 3) + 12;
        }
    }
}