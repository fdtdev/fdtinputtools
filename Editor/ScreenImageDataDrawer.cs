﻿using UnityEditor;
using UnityEngine;

namespace com.FDT.InputTools.Editor
{
    [CustomPropertyDrawer(typeof(ScreenImageData))]
    public class ScreenImageDataDrawer : PropertyDrawer
    {
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            EditorGUI.BeginProperty(position, label, property);
            EditorGUI.BeginChangeCheck();
            EditorGUI.PropertyField(position, property.FindPropertyRelative("img"), label, true);
            if (EditorGUI.EndChangeCheck())
            {
                property.serializedObject.ApplyModifiedProperties();
            }
            EditorGUI.EndProperty();
        }
    }
}