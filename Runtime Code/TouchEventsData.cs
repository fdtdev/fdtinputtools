﻿using UnityEngine;

namespace com.FDT.InputTools
{
    [System.Serializable]
    public class TouchEventsData
    {
        public InputHandlerPlugin callbacksTarget;

        protected GameTouchType _type;

        public float beganTime = 0;

        public Vector3 initPos;

        internal GameTouchType type
        {
            set { _type = value; }
            get { return _type; }
        }

    }
}