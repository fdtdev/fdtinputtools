﻿using System.Collections.Generic;
using com.FDT.Common;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace com.FDT.InputTools
{
    [RequireComponent(typeof(Canvas), typeof(DpCanvasScaler), typeof(GraphicRaycaster))]
    public class InputHandler : MonoBehaviour
    {
        private void OnValidate()
        {
            _canvas = GetComponent<Canvas>();
            if (_canvas != null)
            {
                _canvas.renderMode = RenderMode.ScreenSpaceOverlay;
            }
        }

        public enum SwipeType
        {
            SWIPE4DIR, SWIPE8DIR
        }

        [Header("Settings"), SerializeField] protected Canvas _canvas;
        [SerializeField] protected float _dragTime = 0.1f;
        [SerializeField] protected SwipeType _swipeType;
        [SerializeField, Range(1, 5)] protected int _maxTouchs = 1;
        [SerializeField] protected ScreenData _screenData;
        public ScreenData screenData
        {
            get { return _screenData; }
        }
        public float dragTolerance = 1000f;

        [Space(height: 20, order = 0),
         HelpBox(
             "Each TouchData item is declared for the concurrent fingerID of the index. This provides a way to define in order different calls for each fingerID.\nIf no target is defined, the events will be sent to this gameObject to all the plugins it contains.\nIn that case, specific plugins must be put in a different gameObject.",
             HelpBoxType.Info, 1)]
        public List<TouchEventsData> touchDatas = new List<TouchEventsData>();
        [Space(height:20, order = 0), HelpBox("Mouse emulation only supports 1 finger at a time currently.", HelpBoxType.Warning, 1)]
        public TouchEventsData editorMouseEmulation;
        private void OnEnable()
        {
            _screenData.SetSafeArea(_canvas);
            
            foreach (var t in touchDatas)
            {
                t.type = GameTouchType.NONE;
            }
        }

        protected Vector3 lastMousePos;


        void Update ()
        {
            Camera c = Camera.main;
            // Handle native touch events
            foreach (Touch touch in Input.touches) {
                if (touch.fingerId < _maxTouchs)
                {
                    HandleRealTouch(touch.fingerId, touch.position, c.ScreenToViewportPoint(touch.position),
                        touch.deltaPosition, touch.deltaTime, touch.phase);
                }
            }

            // Simulate touch events from mouse events
            if (Input.touchCount == 0) {
                if (Input.GetMouseButtonDown(0))
                {
                    HandleMouseTouch(Input.mousePosition, c.ScreenToViewportPoint(Input.mousePosition), Input.mousePosition - lastMousePos, Time.deltaTime, TouchPhase.Began);
                    lastMousePos = Input.mousePosition;
                }
                if (Input.GetMouseButton(0)){
                    HandleMouseTouch(Input.mousePosition, c.ScreenToViewportPoint(Input.mousePosition), Input.mousePosition - lastMousePos, Time.deltaTime, TouchPhase.Moved);
                    lastMousePos = Input.mousePosition;
                }
                if (Input.GetMouseButtonUp(0)) {
                    HandleMouseTouch(Input.mousePosition, c.ScreenToViewportPoint(Input.mousePosition), Input.mousePosition - lastMousePos, Time.deltaTime, TouchPhase.Ended);
                    lastMousePos = Input.mousePosition;
                }
            }
        }
        private void HandleRealTouch(int touchFingerId, Vector3 pixelsPosition, Vector3 touchPosition, Vector3 delta, float deltaTime, TouchPhase touchPhase)
        {
            if (touchFingerId < touchDatas.Count)
            {
                HandleTouch(touchFingerId, touchDatas[touchFingerId], pixelsPosition, touchPosition, delta, deltaTime, touchPhase);
            }
        }
        private void HandleMouseTouch(Vector3 pixelsPosition, Vector3 touchPosition, Vector3 delta, float deltaTime, TouchPhase touchPhase)
        {
            HandleTouch(10 , editorMouseEmulation, pixelsPosition, touchPosition, delta, deltaTime, touchPhase);
        }

        private void HandleTouch(int touchFingerId, TouchEventsData data, Vector3 pixelsPosition, Vector3 touchPosition, Vector3 delta, float deltaTime, TouchPhase touchPhase) {
            float min = 0.3f;
            GameObject callbacksTarget = gameObject;
            
            var elapsedTime = Time.realtimeSinceStartup - data.beganTime;
            switch (touchPhase) {
                case TouchPhase.Began:
                    data.initPos = pixelsPosition;
                    data.beganTime = Time.realtimeSinceStartup;
                    if (data.callbacksTarget != null)
                    {
                        callbacksTarget = data.callbacksTarget.gameObject;
                    }
                    ExecuteEvents.Execute<IGameTouchPlugin>(callbacksTarget, null, (x,y)=>x.HandleBegan(touchFingerId, pixelsPosition, touchPosition, this));
                    break;
                case TouchPhase.Moved:
                    if (data.type == GameTouchType.NONE && elapsedTime >= _dragTime)
                    { 
                        Vector3 dist = pixelsPosition - data.initPos;
                        float vel = dist.magnitude / elapsedTime;
                        Debug.Log($"speed: {vel}  |  tolerance: {dragTolerance}");
                        if (vel < dragTolerance)
                        {
                            data.type = GameTouchType.DRAG;
                            if (data.callbacksTarget != null)
                            {
                                callbacksTarget = data.callbacksTarget.gameObject;
                            }
                            ExecuteEvents.Execute<IGameTouchDrag>(callbacksTarget, null, (x,y)=>x.HandleStartDrag(touchFingerId, pixelsPosition, touchPosition, this));
                        }
                        
                    }
                    else if (data.type == GameTouchType.DRAG)
                    {
                        if (data.callbacksTarget != null)
                        {
                            callbacksTarget = data.callbacksTarget.gameObject;
                        }
                        ExecuteEvents.Execute<IGameTouchDrag>(callbacksTarget, null, (x,y)=>x.HandleMoveDrag(touchFingerId, pixelsPosition, touchPosition, this));
                    }
                    break;
                case TouchPhase.Ended:
                    if (data.type == GameTouchType.NONE)
                    {
                        Vector3 dist = pixelsPosition - data.initPos;
                        float vel = dist.magnitude / elapsedTime;
                        Debug.Log($"speed: {vel}  |  tolerance: {dragTolerance}");
                        if (vel >= dragTolerance)
                        {
                            var swipeDir = delta.normalized;
                            data.type = GameTouchType.SWIPE;
                            GameTouchSwipeDir dir = default;
                            if (_swipeType == SwipeType.SWIPE8DIR)
                            {
                                dir = GetSwipe8Dir(swipeDir, min);
                            }
                            else
                            {
                                dir = GetSwipe4Dir(swipeDir, min);
                            }
                            
                            if (data.callbacksTarget != null)
                            {
                                callbacksTarget = data.callbacksTarget.gameObject;
                            }
                            ExecuteEvents.Execute<IGameTouchSwipe>(callbacksTarget, null, (x,y)=>x.HandleSwipe(touchFingerId, dir, pixelsPosition, touchPosition, this));
                        }
                        else
                        {
                            if (data.callbacksTarget != null)
                            {
                                callbacksTarget = data.callbacksTarget.gameObject;
                            }
                            ExecuteEvents.Execute<IGameTouchTap>(callbacksTarget, null, (x,y)=>x.HandleTap(touchFingerId, pixelsPosition, touchPosition, this));
                        }
                    }
                    else if (data.type == GameTouchType.DRAG)
                    {
                        if (data.callbacksTarget != null)
                        {
                            callbacksTarget = data.callbacksTarget.gameObject;
                        }
                        ExecuteEvents.Execute<IGameTouchDrag>(callbacksTarget, null, (x,y)=>x.HandleEndDrag(touchFingerId, pixelsPosition, touchPosition, this));
                    }
                    data.type = GameTouchType.NONE;
                    break;
            }
        }

        protected GameTouchSwipeDir GetSwipe8Dir(Vector3 swipeDir, float min)
        {
            GameTouchSwipeDir dir = default(GameTouchSwipeDir);
            if (swipeDir.y > -min  && swipeDir.y < min && swipeDir.x > min)
            {
                dir = GameTouchSwipeDir.RIGHT;
            }
            else if (swipeDir.x > min &&  swipeDir.y > min)
            {
                dir = GameTouchSwipeDir.UPRIGHT;
            }
            else if (swipeDir.x > -min  && swipeDir.x < min && swipeDir.y > min)
            {
                dir = GameTouchSwipeDir.UP;
            }
            else if (swipeDir.x < -min && swipeDir.y > min)
            {
                dir = GameTouchSwipeDir.UPLEFT;
            }
            else if (swipeDir.y > -min  && swipeDir.y < min && swipeDir.x < -min)
            {
                dir = GameTouchSwipeDir.LEFT;
            }
            else if (swipeDir.x < -min && swipeDir.y < -min)
            {
                dir = GameTouchSwipeDir.DOWNLEFT;
            }
            else if (swipeDir.y < -min && swipeDir.x > -min  && swipeDir.x < min)
            {
                dir = GameTouchSwipeDir.DOWN;
            }
            else if (swipeDir.x > min && swipeDir.y < -min)
            {
                dir = GameTouchSwipeDir.DOWNRIGHT;
            }
            return dir;
        }
        protected GameTouchSwipeDir GetSwipe4Dir(Vector3 swipeDir, float min)
        {
            GameTouchSwipeDir dir = default(GameTouchSwipeDir);
            if (swipeDir.x > min  && Mathf.Abs(swipeDir.x) > Mathf.Abs(swipeDir.y))
            {
                dir = GameTouchSwipeDir.RIGHT;
            }
            else if (swipeDir.y > min  && Mathf.Abs(swipeDir.x) < Mathf.Abs(swipeDir.y))
            {
                dir = GameTouchSwipeDir.UP;
            }
            else if (swipeDir.x < -min  && Mathf.Abs(swipeDir.x) > Mathf.Abs(swipeDir.y))
            {
                dir = GameTouchSwipeDir.LEFT;
            }
            else if (swipeDir.y < -min  && Mathf.Abs(swipeDir.x) < Mathf.Abs(swipeDir.y))
            {
                dir = GameTouchSwipeDir.DOWN;
            }
            return dir;
        }
    }
}

