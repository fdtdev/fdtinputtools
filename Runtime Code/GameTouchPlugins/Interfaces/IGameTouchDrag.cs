﻿using UnityEngine;
using UnityEngine.EventSystems;

namespace com.FDT.InputTools
{
    public interface IGameTouchDrag : IEventSystemHandler
    {
        void HandleStartDrag(int touchFingerId, Vector2 screenPos, Vector3 viewportPos, InputHandler inputHandler);
        void HandleMoveDrag(int touchFingerId, Vector2 screenPos, Vector3 viewportPos, InputHandler inputHandler);
        void HandleEndDrag(int touchFingerId, Vector2 screenPos, Vector3 viewportPos, InputHandler inputHandler);
    }
}