﻿using UnityEngine;
using UnityEngine.EventSystems;

namespace com.FDT.InputTools
{
    public interface IGameTouchSwipe : IEventSystemHandler
    {
        void HandleSwipe(int touchFingerId, GameTouchSwipeDir dir, Vector2 screenPos, Vector3 viewportPos, InputHandler inputHandler);
    }
}