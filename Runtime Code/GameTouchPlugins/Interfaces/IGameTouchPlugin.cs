﻿using UnityEngine;
using UnityEngine.EventSystems;

namespace com.FDT.InputTools
{
    public interface IGameTouchPlugin : IEventSystemHandler
    {
        void HandleBegan(int touchFingerId, Vector2 screenPos, Vector3 viewportPos, InputHandler inputHandler);
    }
}