﻿using UnityEngine;
using UnityEngine.EventSystems;

namespace com.FDT.InputTools
{
    public interface IGameTouchTap : IEventSystemHandler
    {
        void HandleTap(int touchFingerId, Vector2 screenPos, Vector3 viewportPos, InputHandler inputHandler);
    }
}