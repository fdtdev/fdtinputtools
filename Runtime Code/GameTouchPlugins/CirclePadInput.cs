﻿using UnityEngine;
using UnityEngine.UI;

namespace com.FDT.InputTools
{
    public class CirclePadInput : InputHandlerPlugin, IGameTouchTap, IGameTouchDrag, IGameTouchSwipe, IGameTouchPlugin
    {
        public ScreenImageData circleData;
        public ScreenImageData pointData;
        public ScreenImageData freePointData;
        
        public float maxCircleDistance;

        public bool circleMoves = true;
        public float maxMovementScreenPercent = 1f;
        protected float maxMovement;
  
        public int MovementCircleIdx = -1;
        
        public override void OnEnable()
        {
            maxMovement = (Screen.width) * maxMovementScreenPercent / 100.0f;
        }

        public override void OnDisable()
        {
            
        }
        public void HandleStartDrag(int touchFingerId, Vector2 screenPos, Vector3 viewportPos, InputHandler inputHandler)
        {
            freePointData.screenPos = screenPos;
            if (MovementCircleIdx == -1)
            {
                MovementCircleIdx = touchFingerId;
                StartMovement(screenPos, inputHandler);
            }
        }
        private void StartMovement(Vector2 screenPos, InputHandler inputHandler )
        {
            var screenData = inputHandler.screenData;
            pointData.screenPos = screenPos;
            freePointData.screenPos = screenPos;
            screenPos.x = screenData.ClampScreenX(screenPos.x);
            screenPos.y = screenData.ClampScreenY(screenPos.y);
            circleData.screenPos = screenPos;
            
            circleData.img.enabled = true;
            pointData.img.enabled = true;
        }
        public void HandleMoveDrag(int touchFingerId, Vector2 screenPos, Vector3 viewportPos, InputHandler inputHandler)
        {
            freePointData.screenPos = screenPos;
            
            if (MovementCircleIdx == touchFingerId)
            {
                ContinueMovement(screenPos, inputHandler);
            }
        }
        
        private void ContinueMovement(Vector2 screenPos, InputHandler inputHandler)
        {
            var screenData = inputHandler.screenData;
            var maxCircleDist = maxCircleDistance * screenData.scaleFactor;
            freePointData.screenPos = screenPos;
            pointData.screenPos = screenPos;
            circleData.img.enabled = true;
            pointData.img.enabled = true;

            var dist = pointData.screenPos - circleData.screenPos;
            if (dist.magnitude > maxCircleDist)
            {
                if (circleMoves)
                {
                    circleData.screenPos = pointData.screenPos - (dist.normalized * maxCircleDist);
                    var cP = circleData.screenPos;
                    cP.x = screenData.ClampScreenX(cP.x);
                    cP.y = screenData.ClampScreenY(cP.y);
                    circleData.screenPos = cP;
                    dist = pointData.screenPos - circleData.screenPos;
                }
                pointData.screenPos = circleData.screenPos + (dist.normalized * maxCircleDist);
            }
            dist = pointData.screenPos - circleData.screenPos;
            float movX = Mathf.InverseLerp(0, maxCircleDist, Mathf.Abs(dist.x));
            float movY = Mathf.InverseLerp(0, maxCircleDist, Mathf.Abs(dist.y));
            movX *= Mathf.Sign(dist.x);
            movY *= Mathf.Sign(dist.y);
            _gameInputRuntimeAsset.horizontal = movX;
            _gameInputRuntimeAsset.vertical = movY;
        }

        public void HandleEndDrag(int touchFingerId, Vector2 screenPos, Vector3 viewportPos, InputHandler inputHandler)
        {
            freePointData.screenPos = screenPos;
            
            if (touchFingerId == MovementCircleIdx)
            {
                MovementCircleIdx = -1;
                EndMovement();
            }
        }

        public Text swipe;
        public void HandleTap(int touchFingerId, Vector2 screenPos, Vector3 viewportPos, InputHandler inputHandler)
        {
            freePointData.screenPos = screenPos;
            _gameInputRuntimeAsset.touchRequested = true;
        }
        public void HandleSwipe(int touchFingerId, GameTouchSwipeDir dir, Vector2 screenPos, Vector3 viewportPos, InputHandler inputHandler)
        {
            swipe.gameObject.SetActive(true);
            swipe.text = $"swipe {dir}";
        }

        public void HandleBegan(int touchFingerId, Vector3 screenPos, Vector3 viewportPos, InputHandler inputHandler)
        {
            
        }

        private void EndMovement()
        {
            circleData.img.enabled = false;
            pointData.img.enabled = false;
            _gameInputRuntimeAsset.horizontal = 0;
            _gameInputRuntimeAsset.vertical = 0;
        }

        public void HandleBegan(int touchFingerId, Vector2 screenPos, Vector3 viewportPos, InputHandler inputHandler)
        {
            
        }
    }
}