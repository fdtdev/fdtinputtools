﻿namespace com.FDT.InputTools
{
    public enum GameTouchType
    {
        NONE = 0,
        DRAG = 1,
        TAP = 2,
        SWIPE = 3
    }
}