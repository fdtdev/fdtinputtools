﻿using UnityEngine;
namespace com.FDT.InputTools
{
    [CreateAssetMenu(menuName = "FDT/InputTools/GameInputRuntimeAsset", fileName = "GameInputRuntime")]
    public class GameInputRuntimeAsset : ResetScriptableObject
    {
        public float horizontal = 0;
        public float vertical = 0;
        public bool touchRequested = false;
        public bool swipeRequested = false;
        public GameTouchSwipeDir swipeDir;
    }
}