﻿using UnityEngine;

namespace com.FDT.InputTools
{
    public abstract class InputHandlerPlugin : MonoBehaviour
    {
        [SerializeField] protected GameInputRuntimeAsset _gameInputRuntimeAsset;

        public GameInputRuntimeAsset GameInputRuntimeAsset => _gameInputRuntimeAsset;

        public virtual void HandleBegan(int touchFingerId, Vector2 screenPos, Vector3 viewportPos)
        {
        }

        protected virtual void OnInit()
        {
            
        }
        public abstract void OnEnable();
        public abstract void OnDisable();
    }
}