﻿using UnityEngine;
using UnityEngine.UI;

namespace com.FDT.InputTools
{
    [System.Serializable]
    public class ScreenImageData
    {
        public Image img;
        [SerializeField] protected Vector2 _screenPos;
        public Vector2 screenPos
        {
            get { return _screenPos; }
            set
            {
                _screenPos = value;
                _viewportPos = new Vector2(_screenPos.x / Screen.width, _screenPos.y / Screen.height);
                _canvasPos = img.canvas.ViewportToCanvasPosition(_viewportPos);
                img.rectTransform.anchoredPosition = _canvasPos;
            }
        }
        [SerializeField] protected Vector2 _viewportPos;
        public Vector2 viewportPos
        {
            get { return _viewportPos; }
        }
        [SerializeField] Vector3 _canvasPos;
        public Vector3 canvasPos
        {
            get { return _canvasPos; }
        }
    }
}