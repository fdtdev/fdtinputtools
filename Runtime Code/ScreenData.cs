﻿using com.FDT.Common;
using UnityEngine;

namespace com.FDT.InputTools
{
    [System.Serializable]
    public class ScreenData
    {
        [MinMaxRange(0.0f, 1.0f)] public Vector2 widthSafeArea;
        [MinMaxRange(0.0f, 1.0f)] public Vector2 heightSafeArea;
        public float minX;
        public float maxX;
        public float minY;
        public float maxY;

        public float scaleFactor;

        public float ClampScreenX(float v)
        {
            return Mathf.Clamp(v, minX, maxX);
        }

        public float ClampScreenY(float v)
        {
            return Mathf.Clamp(v, minY, maxY);
        }
        public void SetSafeArea(Canvas canvas)
        {
            minX = Screen.width * widthSafeArea.x;
            maxX = Screen.width * widthSafeArea.y;
            minY = Screen.height * heightSafeArea.x;
            maxY = Screen.height * heightSafeArea.y;
            scaleFactor = canvas.scaleFactor;
        }
    }
}